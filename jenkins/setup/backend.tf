terraform {
  backend "s3" {
    bucket = "nodejs-jenkins-terraform"
    key    = "jenkins.terraform.tfstate"
    region = "us-east-1"
  }
}

