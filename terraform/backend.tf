terraform {
  backend "s3" {
    bucket = "nodejs-jenkins-terraform"
    key = "nodejs-jenkins-terraform.tfstate"
    region = "us-east-1"
  }
}